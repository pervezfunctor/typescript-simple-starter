import { map, pipe } from 'utils'

console.log(
  'squares: ',
  pipe(
    [1, 2, 3, 4],
    map(x => x * x),
  ),
)

{
  const mdl = module as any
  if (mdl.hot !== undefined) {
    mdl.hot.accept()
  }
}
